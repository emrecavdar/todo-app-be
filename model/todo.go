package model

import (
	"errors"
	"time"
)

type Todo struct {
	Id          int    `json:"id" pact:"example=1"`
	IsCompleted bool   `json:"isCompleted" pact:"example=false"`
	Text        string `json:"text" pact:"ex ample=first test text"`
	CreateDate  time.Time
}

var (
	ErrNotFound = errors.New("not found")
	ErrEmpty    = errors.New("empty string")
)
