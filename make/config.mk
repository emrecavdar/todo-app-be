  
SHELL = "/bin/bash"

export PATH := $(PWD)/pact/bin:$(PATH)
export PATH
export PROVIDER_NAME = GoUserService
export CONSUMER_NAME = GoAdminService
export PACT_DIR = $(PWD)/pacts
export LOG_DIR = $(PWD)/log