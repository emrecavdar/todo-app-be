package service

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
	"todo-app-go-emre/model"
	"todo-app-go-emre/repository"
)

var todoRepository = &repository.TodoRepository{
	Todos: map[string]*model.Todo{},
}

func GetTodo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	a := strings.Split(r.URL.Path, "/")
	id, _ := strconv.Atoi(a[len(a)-1])

	todo, err := todoRepository.ByID(id)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
	} else {
		w.WriteHeader(http.StatusOK)
		resBody, _ := json.Marshal(todo)
		w.Write(resBody)
	}
}

func AddTodoItem(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	reqBody, _ := ioutil.ReadAll(r.Body)
	var todo model.Todo
	json.Unmarshal(reqBody, &todo)
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)

	} else if r.Method != "OPTIONS" {
		todo2, err := todoRepository.AddItem(&model.Todo{
			Id:          todo.Id,
			Text:        todo.Text,
			IsCompleted: todo.IsCompleted,
			CreateDate:  time.Now(),
		})
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusOK)
			resBody, _ := json.Marshal(todo2)
			w.Write(resBody)
		}
	}

}
func GetTodos(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	w.WriteHeader(http.StatusOK)
	resBody, _ := json.Marshal(todoRepository.GetTodos())
	w.Write(resBody)
}

func UpdateTodo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)

	} else {
		reqBody, _ := ioutil.ReadAll(r.Body)
		var todo model.Todo
		json.Unmarshal(reqBody, &todo)
		a := strings.Split(r.URL.Path, "/")
		id, _ := strconv.Atoi(a[len(a)-1])

		todo, err := todoRepository.UpdateItem(todo, id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusOK)
			resBody, _ := json.Marshal(todo)
			w.Write(resBody)
		}
	}

}
func DeleteTodo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	if r.Method == "OPTIONS" {
		w.WriteHeader(http.StatusOK)
	} else {
		a := strings.Split(r.URL.Path, "/")
		id, _ := strconv.Atoi(a[len(a)-1])

		todo, err := todoRepository.DeleteItem(id)
		if err != nil {
			w.WriteHeader(http.StatusNotFound)
		} else {
			w.WriteHeader(http.StatusOK)
			resBody, _ := json.Marshal(todo)
			w.Write(resBody)
		}
	}

}

func GetHTTPHandler() *http.ServeMux {
	mux := http.NewServeMux()
	return mux
}
