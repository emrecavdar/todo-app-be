package service

import (
	"fmt"
	"github.com/pact-foundation/pact-go/dsl"
	"github.com/pact-foundation/pact-go/types"
	"github.com/phayes/freeport"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"testing"
)

func TestPactProvider(t *testing.T) {
	go startInstrumentedProvider()
	pact := createPact()

	// Verify the Provider - Tag-based Published Pacts for any known consumers
	_, err := pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL: fmt.Sprintf("http://127.0.0.1:%d", port),
		Tags:            []string{"master"},
		PactURLs:        []string{filepath.FromSlash(fmt.Sprintf("%s/goadminservice-gouserservice.json", os.Getenv("PACT_DIR")))},
		ProviderVersion: "1.0.0",
	})

	if err != nil {
		t.Log(err)
	}
}

func startInstrumentedProvider() {
	mux := GetHTTPHandler()

	ln, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()

	log.Printf("API starting: port %d (%s)", port, ln.Addr())
	log.Printf("API terminating: %v", http.Serve(ln, mux))

}

var dir, _ = os.Getwd()
var pactDir = fmt.Sprintf("%s/../../pacts", dir)
var logDir = fmt.Sprintf("%s/log", dir)
var port, _ = freeport.GetFreePort()

func createPact() dsl.Pact {
	return dsl.Pact{
		Provider: "GoTodoService",
		LogDir:   logDir,
		LogLevel: "INFO",
		PactDir:  pactDir,
	}
}
