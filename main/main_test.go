package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a App

func TestMain(m *testing.M) {
	a = App{}
	a.Initialize()

	code := m.Run()

	os.Exit(code)
}
func TestCreateTodos(t *testing.T) {
	var jsonStr = []byte(`{"id":1, "isCompleted": false,"text":"test todos"}`)
	req, _ := http.NewRequest("POST", "/todo", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)
	if m["text"] != "test todos" {
		t.Errorf("Expected todos name to be ‘test todos’. Got ‘%v’", m["text"])
	}
	if m["isCompleted"] != false {
		t.Errorf("Expected todos price to be ‘false’. Got ‘%v’", m["isCompleted"])
	}
}
func TestUpdateTodo(t *testing.T) {
	//Given
	var itemToBeAdded = []byte(`{"id":1, "isCompleted": false,"text":"test text"}`)
	req, _ := http.NewRequest("POST", "/todo", bytes.NewBuffer(itemToBeAdded))
	req.Header.Set("Content-Type", "application/json")
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	req, _ = http.NewRequest("GET", "/todo/detail/1", nil)
	response = executeRequest(req)
	var originalTodo map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &originalTodo)
	//When
	var jsonStr = []byte(`{"id":1, "isCompleted": true,"text":"test todos updated"}`)
	req, _ = http.NewRequest("PUT", "/todo/sync/1", bytes.NewBuffer(jsonStr))
	req.Header.Set("Content-Type", "application/json")

	response = executeRequest(req)
	//Then
	checkResponseCode(t, http.StatusOK, response.Code)

	var m map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &m)

	if m["text"] == originalTodo["text"] {
		t.Errorf("Expected the text to change from '%v' to '%v'. Got '%v'", originalTodo["text"], m["text"], m["text"])
	}

	if m["isCompleted"] == originalTodo["isCompleted"] {
		t.Errorf("Expected the isCompleted to change from '%v' to '%v'. Got '%v'", originalTodo["isCompleted"], m["isCompleted"], m["isCompleted"])
	}
}

func TestDeleteTodoItem(t *testing.T) {

	var itemToBeAdded = []byte(`{"id":1, "isCompleted": false,"text":"test text"}`)
	req, _ := http.NewRequest("POST", "/todo", bytes.NewBuffer(itemToBeAdded))
	req.Header.Set("Content-Type", "application/json")
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("GET", "/todo/detail/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("DELETE", "/todo/1", nil)
	response = executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	req, _ = http.NewRequest("GET", "/todo/detail/1", nil)
	response = executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
