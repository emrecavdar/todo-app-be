package main

import (
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"todo-app-go-emre/service"
)

type App struct {
	Router *mux.Router
}

func (a *App) Initialize() {
	a.Router = mux.NewRouter()
	a.initializeRoutes()
}
func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(":8010", a.Router))
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/todos", service.GetTodos).Methods("GET", "OPTIONS")
	a.Router.HandleFunc("/todo", service.AddTodoItem).Methods("POST", "OPTIONS")
	a.Router.HandleFunc("/todo/detail/{id:[0-9]+}", service.GetTodo).Methods("GET", "OPTIONS")
	a.Router.HandleFunc("/todo/sync/{id:[0-9]+}", service.UpdateTodo).Methods("PUT", "OPTIONS")
	a.Router.HandleFunc("/todo/{id:[0-9]+}", service.DeleteTodo).Methods("DELETE", "OPTIONS")
}
