FROM golang:1.15-alpine as build


RUN mkdir -p /go/src/gitlab.com/emrecavdar/todo-app-be
WORKDIR /go/src/gitlab.com/emrecavdar/todo-app-be

# Copy the Go App to the container
COPY . .

#RUN go mod init

RUN go build -o todo-app /go/src/gitlab.com/emrecavdar/todo-app-be/main/main.go /go/src/gitlab.com/emrecavdar/todo-app-be/main/app.go

FROM alpine

COPY --from=build go/src/gitlab.com/emrecavdar/todo-app-be /todo-app

EXPOSE 8010

RUN chmod +x todo-app
CMD [ "./todo-app/todo-app" ]