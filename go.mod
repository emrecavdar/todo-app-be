module todo-app-go-emre

go 1.15

require (
	github.com/gorilla/mux v1.8.0
	github.com/pact-foundation/pact-go v1.5.1
	github.com/phayes/freeport v0.0.0-20180830031419-95f893ade6f2
)
