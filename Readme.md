# Todo-App Go Project

This is a todo list application. Thanks to this application, users can follow their todos.
http://104.236.39.102:3002/

# API's

##(GET)/todos:
Return all todos

##(GET)/todo/{todo-id}
Return detail of todo

##(POST)/todo
Create todo item. Client should send data in body.

##(PUT)/todo/sync/{todo-id}
Update the todo.Client should send data in body

##(DELETE)/todo/{todo-id}
Delete todo item

## Installation and Setup Instructions

Clone down this repository. You will need node and npm installed globally on your machine.



To Start Server:
### `go run /main/main.go /main/app.go`
