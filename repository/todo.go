package repository

import (
	"strconv"
	"todo-app-go-emre/model"
)

type TodoRepository struct {
	Todos map[string]*model.Todo
}

func (t *TodoRepository) GetTodos() []model.Todo {
	var response []model.Todo

	for _, user := range t.Todos {
		response = append(response, *user)
	}

	return response
}

func (t *TodoRepository) ByID(ID int) (*model.Todo, error) {
	for _, todo := range t.Todos {
		if todo.Id == ID {
			return todo, nil
		}
	}
	return nil, model.ErrNotFound
}
func (t *TodoRepository) AddItem(todo *model.Todo) (*model.Todo, error) {
	stringValue := strconv.Itoa(todo.Id)
	t.Todos[stringValue] = todo
	return todo, nil
}

func (t *TodoRepository) DeleteItem(id int) (*model.Todo, error) {
	todo, err := t.ByID(id)
	if err != nil {
		return nil, model.ErrNotFound
	}
	delete(t.Todos, strconv.Itoa(todo.Id))
	return todo, nil
}
func (t *TodoRepository) UpdateItem(todo model.Todo, id int) (model.Todo, error) {
	t.Todos[strconv.Itoa(id)] = &todo
	return todo, nil
}
